@isTest
public class TestAddProductsController {
    static public String pbName = 'testBook';

    @TestSetup
    static public void makeData() {
        Pricebook2 pb = new Pricebook2(Name=TestAddProductsController.pbName, IsActive=true);
        insert pb;

        Product2[] products = new List<Product2>();
        for(Integer i=0; i < 5; i++) {
            Product2 p = new Product2(Name='product-'+i, 
                                    ProductCode='P'+i,
                                    Port__c='port 1',
                                    Port_Code__c='pc1',
                                    Incoterm__c='CIF',
                                    IsActive=true);
            products.add(p);
        }
        insert products;

        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry[] pbes = new List<PricebookEntry>();
        for(Product2 p : products) {
            PricebookEntry pbe = new PricebookEntry(Product2Id=p.Id, Pricebook2Id=pricebookId, UnitPrice=1000, IsActive=true);
            pbes.add(pbe);
        }
        insert pbes;
        pbes.clear();

        Integer i = 1;
        for(Product2 p : products) {
            PricebookEntry pbe = new PricebookEntry(Product2Id=p.Id, 
                                                    Pricebook2Id=pb.Id,
                                                    IsActive=true,
                                                    UnitPrice=1000 + i);
            pbes.add(pbe);

            i++;
        }
        insert pbes;

        Date closeDate = Date.today();
        closeDate = closeDate.addDays(30);
        Opportunity opp = new Opportunity(Name='testOpp', 
                                    StageName='Prospecting', 
                                    Pricebook2Id=pb.Id,
                                    CloseDate=closeDate);
        insert opp;
    }

    @isTest
    public static void testPricebookId() {
        Id pbId = [SELECT Id FROM Pricebook2 LIMIT 1].Id;
        Id gotId = AddProductsController.getPricebookId(TestAddProductsController.pbName);

        System.assertEquals(pbId, gotId);
    }

    @isTest
    public static void testGetAllProducts() {
        Id pbId = [SELECT Id FROM Pricebook2 LIMIT 1].Id;
        PricebookEntry[] products = AddProductsController.getAllProducts(pbId);

        System.assertEquals(5, products.size());
    }

    @isTest
    public static void testGetProducts() {
        String port = 'port 1';
        String productCode = 'P1';
        String incoterm = 'CIF';
        Id pbId = [SELECT Id FROM Pricebook2 LIMIT 1].Id;

        PricebookEntry[] pbe = AddProductsController.getProducts(pbId, port, productCode, incoterm);
        System.assertEquals(1, pbe.size());
    }

    @isTest
    public static void testGetIncoterms() {
        String[] incoterms = AddProductsController.getIncoterms();
        // System.assertEquals(1, 1);
    }

    @isTest
    static public void testAddProducts() {
        Id Opportunity = [SELECT Id FROM Opportunity LIMIT 1].Id;
        Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE Name = 'testBook'];
        
        PricebookEntry[] pbes = 
            [SELECT Id, Name, Product2Id, Pricebook2Id, UnitPrice, ProductCode 
            FROM PricebookEntry 
            WHERE Pricebook2Id = :pb.Id];
        
        DataObject[] dos = new List<DataObject>();
        for(PricebookEntry pbe : pbes) {
            DataObject o = new DataObject();

            o.Id = pbe.Id;
            o.Name = pbe.Name;
            o.Product2Id = pbe.Product2Id;
            o.PriceBook2Id = pbe.Pricebook2Id;
            o.UnitPrice = pbe.UnitPrice;
            o.ProductCode = pbe.ProductCode;
            o.quantity = 100;
            o.ServiceDate = Date.valueOf('2090-01-01');
            o.isChecked = true;
            o.description = 'lalalalalala';

            dos.add(o);
        }

        String json = JSON.serialize(dos);
        AddProductsController.addProducts(json, Opportunity);
    }

    class DataObject {
        Id Id {get;set;}
        String Name {get;set;}
        String Product2Id {get;set;}
        String Pricebook2Id {get;set;}
        Decimal UnitPrice {get;set;}
        String ProductCode {get;set;}
        Decimal quantity {get;set;}
        Date ServiceDate {get;set;}
        Boolean isChecked {get;set;}
        String description {get;set;}
    }
}