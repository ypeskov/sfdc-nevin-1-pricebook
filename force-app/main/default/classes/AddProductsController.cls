public with sharing class AddProductsController {
    @AuraEnabled
    public static PricebookEntry[] getAllProducts(String pricebookId) {
        String query = 'SELECT Id, Name, ProductCode, Product2.Port__c, ' + 
                'Product2.Incoterm__c, Product2.Port_Code__c, UnitPrice ' +
                'FROM PricebookEntry ' +
                'WHERE Pricebook2Id = \'' + String.escapeSingleQuotes(pricebookId) + '\' ' +
                'ORDER BY Name';

        PricebookEntry[] pbe = Database.query(query);

        return pbe;
    }

    @AuraEnabled
    public static String[] getIncoterms() {
        String[] products = new List<String>();
        Schema.DescribeFieldResult field = Product2.Incoterm__c.getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues()) {
            products.add(f.getLabel());
        }

        return products;
    }

    @AuraEnabled
    public static PricebookEntry[] getProducts(String pricebookId, 
                                            String port, 
                                            String productCode,
                                            String incoterm) {
        String conditionPart1 = '', 
                conditionPart2 = '', 
                conditionPart3 = '';

        if (port.length() > 0) {
            conditionPart1 += ' AND Product2.Port__c LIKE \'%' + String.escapeSingleQuotes(port) + '%\' ';
        }

        if (productCode.length() > 0) {
            conditionPart3 = ' AND ProductCode LIKE \'%' + String.escapeSingleQuotes(productCode) + '%\' ';
        }

        String condition4 = ' AND Product2.Incoterm__c LIKE \'%' + String.escapeSingleQuotes(incoterm) + '%\'';

        String fullCondition = conditionPart1 + conditionPart2 + conditionPart3 + condition4;

        String query = 'SELECT Id, Name, ProductCode, Product2.Port__c, ' +
                        'Product2.Incoterm__c, Product2.Port_Code__c, UnitPrice ' +
                        'FROM PricebookEntry ' +
                        'WHERE Pricebook2Id = \'' + 
                        String.escapeSingleQuotes(pricebookId) + '\' ' +
                        fullCondition +
                        'ORDER BY Name';

        PricebookEntry[] pbe = Database.query(query);
        
        return pbe;
    }

    @AuraEnabled
    public static String getPricebookId(String pricebookName) {
        Pricebook2 pb =  [SELECT Id FROM Pricebook2 WHERE Name = :pricebookName LIMIT 1];

        return pb.Id;
    }

    @AuraEnabled
    public static Boolean addProducts(String entries, String oppId) {
        OpportunityLineItem[] items = new List<OpportunityLineItem>();

        PYL_OppLine[] lines = PYL_OppLine.parse(entries);

        for(PYL_OppLine line : lines) {
            OpportunityLineItem item = new OpportunityLineItem();

            item.OpportunityId = oppId;
            item.PricebookEntryId  = line.Id;
            item.Description = line.description;
            item.UnitPrice = line.UnitPrice;
            item.Quantity = line.quantity;
            item.ServiceDate = Date.valueOf(line.ServiceDate);

            items.add(item);
        }

            insert items;

        return true;
    }


}