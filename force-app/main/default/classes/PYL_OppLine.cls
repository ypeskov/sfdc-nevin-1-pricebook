public class PYL_OppLine {

	public String Id {get;set;} 
	public String Name {get;set;} 
	public String ProductCode {get;set;} 
	public Integer UnitPrice {get;set;} 
	public Boolean isChecked {get;set;} 
	public Decimal quantity {get;set;}
	public String ServiceDate {get;set;}
	public String description {get;set;} 

	public PYL_OppLine(JSONParser parser) {
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					if (text == 'Id') {
						Id = parser.getText();
					} else if (text == 'Name') {
						Name = parser.getText();
					} else if (text == 'ProductCode') {
						ProductCode = parser.getText();
					} else if (text == 'UnitPrice') {
						UnitPrice = parser.getIntegerValue();
					} else if (text == 'isChecked') {
						isChecked = parser.getBooleanValue();
					} else if (text == 'quantity') {
						quantity = parser.getDecimalValue();
					} else if (text == 'ServiceDate') {
						ServiceDate = parser.getText();
					} else if (text == 'description') {
						description = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'PYL_OppLine consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
	
	
	public static List<PYL_OppLine> parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return arrayOfPYL_OppLine(parser);
	}
	
	public static void consumeObject(System.JSONParser parser) {
		Integer depth = 0;
		do {
			System.JSONToken curr = parser.getCurrentToken();
			if (curr == System.JSONToken.START_OBJECT || 
				curr == System.JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == System.JSONToken.END_OBJECT ||
				curr == System.JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}
	

    private static List<PYL_OppLine> arrayOfPYL_OppLine(System.JSONParser p) {
        List<PYL_OppLine> res = new List<PYL_OppLine>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new PYL_OppLine(p));
        }
        return res;
    }
}