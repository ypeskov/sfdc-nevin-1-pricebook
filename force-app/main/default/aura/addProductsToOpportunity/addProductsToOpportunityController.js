({
    doInit: function (cmp, evt, hlp) {
        const pricebookName = cmp.get('v.pricebookName');
        const action = cmp.get('c.getPricebookId');
        action.setParams({
            pricebookName
        });
        action.setCallback(this, response => {
            const state = response.getState();
            if ('SUCCESS' === state) {
                cmp.set('v.pricebookId', response.getReturnValue());
            } else if ('INCOMPLETE' === state) {

            } else if ('ERROR' === state) {
                hlp.processResponseError(response);
            }
        });
        $A.enqueueAction(action);

        hlp.getIncoterms(cmp);
    },

    resetFilters: function(cmp, evt, hlp) {
        cmp.set('v.port', '');
        cmp.set('v.productCode', '');

        const productsTable = cmp.find('productsTable');
        productsTable.resetProducts();
    },

    showAllProducts: function(cmp, evt, hlp) {
        cmp.set('v.port', '');
        cmp.set('v.productCode', '');
        
        const productsTable = cmp.find('productsTable');
        productsTable.showAllProducts();
    },

    showPopup: function(cmp, evt, hlp) {
        hlp.showPopupHelper(cmp, 'modaldialog', 'slds-fade-in-');
        hlp.showPopupHelper(cmp, 'backdrop', 'slds-backdrop--');
    },

    hidePopup: function (cmp, event, helper) {
        helper.hidePopupHelper(cmp, 'modaldialog', 'slds-fade-in-');
        helper.hidePopupHelper(cmp, 'backdrop', 'slds-backdrop--');
    },

    processSearchChange: function(cmp, evt, hlp) {
        const port = cmp.get('v.port');
        const productCode = cmp.get('v.productCode');
        const inputDelay = cmp.get('v.inputDelay');
        const incoterm = cmp.find('incoterms').get('v.value');

        let timer = cmp.get('v.timer');
        clearTimeout(timer);

        timer = setTimeout(() => {
            const productsTable = cmp.find('productsTable');
            if (productsTable) {
                productsTable.searchProducts(port, productCode, incoterm);
            }

            clearTimeout(timer);
            cmp.set('v.timer', null);
        }, inputDelay);

        cmp.set('v.timer', timer);
    },

})