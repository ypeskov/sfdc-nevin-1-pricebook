({
    showPopupHelper: function(component, componentId, className){
        let modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },

    hidePopupHelper: function(component, componentId, className) {
        let modal = component.find(componentId);
        $A.util.addClass(modal, className + 'hide');
        $A.util.removeClass(modal, className + 'open');
    },

    processResponseError: function(response) {
        let errors = response.getError();
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }

        console.error(message);
    },
    
    getIncoterms: function(cmp) {
        const action = cmp.get('c.getIncoterms');
        
        action.setCallback(this, response => {
            const state = response.getState();
            if ('SUCCESS' === state) {
                const incoterms = response.getReturnValue();
                cmp.set('v.incoterms', incoterms);
                window.setTimeout(
                    $A.getCallback(function() {
                    cmp.find("incoterms").set("v.value", incoterms[0]);
                }));
            } else if ('INCOMPLETE' === state) {

            } else if ('ERROR' === state) {
                hlp.processResponseError(response);
            }
        });
        $A.enqueueAction(action);
    },
})