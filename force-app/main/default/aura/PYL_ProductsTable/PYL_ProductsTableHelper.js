({
    getProducts: function(cmp, pricebookId, port, productCode, incoterm) {
        const oppId = cmp.get('v.oppId');

        let action = cmp.get('c.getProducts');
        action.setParams({
            pricebookId,
            port,
            productCode,
            incoterm
        });
        action.setCallback(this, response => {
            let state = response.getState();
            if (state === 'SUCCESS') {
                let entries = response.getReturnValue();
                entries = this.prepareProducts(entries);
                cmp.set('v.products', entries);
            } else if (state === 'INCOMPLETE') {

            } else if (state === 'ERROR') {
                this.processResponseError(response);
            }
        });
        $A.enqueueAction(action);
    },

    prepareProducts: function(entries) {
        for(let i in entries) {
            entries[i].isChecked = false;
            entries[i].quantity = 0;
            entries[i].description = '';
            entries[i].dateError = false;
        }

        return entries;
    },

    getAllProducts: function(cmp, hlp, pricebookId) {
        let action = cmp.get('c.getAllProducts');
        action.setParams({pricebookId});
        action.setCallback(this, response => {
            let state = response.getState();
            if (state === 'SUCCESS') {
                let entries = response.getReturnValue();
                entries = this.prepareProducts(entries);
                cmp.set('v.products', entries);
            } else if (state === 'INCOMPLETE') {

            } else if (state === 'ERROR') {
                this.processResponseError(response);
            }
        });
        $A.enqueueAction(action);
    },

    addProducts: function(cmp, products) {
        const action = cmp.get('c.addProducts');
        const oppId = cmp.get('v.oppId');
        
        let isError = false;
        for(let i in products) {
            products[i].quantity = parseFloat(products[i].quantity);

            if (products[i].quantity > 0 && !products[i].ServiceDate) {
                products[i].dateError = true;
            (isError) = true;
            } else if (products[i].quantity > 0 && products[i].ServiceDate.length > 0) {
                products[i].dateError = false;
            }
        }

        if (isError) {
            cmp.set('v.products', products);
        } else {
            const productsToAdd = JSON.stringify(products.filter(product => product.isChecked === true));

            action.setParams({
                entries: productsToAdd,
                oppId
            });
            action.setCallback(this, response => {
                let state = response.getState();
                if (state === 'SUCCESS') {
                    $A.get('e.force:refreshView').fire();
    
                    setTimeout(() => {
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "type": 'success',
                            "message": "Products were added."
                        });
                        toastEvent.fire();
                    }, 500);
    
                    let event = cmp.getEvent('hidePopup');
                    event.fire();
                } else if (state === 'INCOMPLETE') {
    
                } else if (state === 'ERROR') {
                    this.processResponseError(response);
                }
            });
            
            $A.enqueueAction(action);    
        }        
    },

    processResponseError: function(response) {
        let errors = response.getError();
        let message = 'Unknown error';
        if (errors && Array.isArray(errors) && errors.length > 0) {
            message = errors[0].message;
        }

        console.error(message);
    },
})