({
    doInit: function(cmp, evt, hlp) {
        
    },

    searchProducts: function(cmp, evt, hlp) {
        const searchLength = cmp.get('v.searchLength');

        let params = evt.getParam('arguments');
        if (params) {
            const port = params.port;
            const productCode = params.productCode;
            const incoterm = params.incoterm;
            const pricebookId = cmp.get('v.pricebookId');
            
            if (port.length >= searchLength || productCode.length >= searchLength) {
                hlp.getProducts(cmp, pricebookId, port, productCode, incoterm);
            } else if (port.length === 0 && productCode.length === 0) {
                cmp.set('v.products', []);
            }
        }
    
    },

    resetProducts: function(cmp) {
        cmp.set('v.products', []);
    },

    showAllProducts: function(cmp, evt, hlp) {
        const pricebookId = cmp.get('v.pricebookId');
        hlp.getAllProducts(cmp, hlp, pricebookId);
    },

    hidePopup: function(cmp, evt, hlp) {
        let event = cmp.getEvent('hidePopup');
        event.fire();
    },

    addProductsToOpp: function(cmp, evt, hlp) {
        hlp.addProducts(cmp, cmp.get('v.products'));
    },

    validate: function(cmp, evt, hlp) {
        let products = cmp.get('v.products');

        for(let i in products) {
            products[i].quantity = parseFloat(products[i].quantity);

            if (products[i].quantity > 0 && !products[i].ServiceDate) {
                products[i].dateError = true;
            } else if (products[i].quantity > 0 && products[i].ServiceDate.length > 0) {
                products[i].dateError = false;
            }
        }

        cmp.set('v.products', products);
    }
})